#!/usr/bin/env python

import mmap
import argparse
import time


def create_args():
    parser = argparse.ArgumentParser(description='Реализация tail')
    parser.add_argument('filepath', type=argparse.FileType('r'))
    parser.add_argument('-f', action='store_true', default=False,
                        help='Ожидать появление новых данных')
    parser.add_argument('-n', type=int, default=10,
                        help='Количество выводимых строк')
    parser.add_argument('-s', type=int, default=1,
                        help='Частота обновления (работает только с -f)')

    return parser.parse_args()


def tail(filepath, f, n, s):
    entries = []
    mapping = mmap.mmap(filepath.fileno(), 0, prot=mmap.ACCESS_READ)
    position = size = mapping.size()
    counter = 0

    while counter <= n:
        entry = mapping.rfind(b'\n', 0, position)
        position = entry
        if counter >= 1:
            entries.insert(0, position + 1)
        counter += 1

    for position in entries:
        mapping.seek(position)
        print(mapping.readline()[:-1].decode())

    if f:
        while True:
            new_size = mmap.mmap(filepath.fileno(), 0,
                                 prot=mmap.ACCESS_READ).size()
            if new_size != size:
                tail(filepath, f, n, s)
            time.sleep(s)


if __name__ == '__main__':
    parsed_args = create_args()
    tail(**vars(parsed_args))
