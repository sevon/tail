Instalation:
```
git clone git@gitlab.com:sevon/tail.git && cd tail
```

Usage:

```
./tail <path to file>
```

optional arguments:
```
-h, --help  show this help message and exit
-f          Ожидать появление новых данных
-n N        Количество выводимых строк
-s S        Частота обновления (работает только с -f)
```